<?php

namespace M2\FirstOrderMinimum\Helper;

use Magento\Checkout\Model\Session;
use \Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class Data
 * @package TradeGecko\Api\Helper
 */
class Data extends AbstractHelper
{
    const MINIMUM_SUBORDER_FOR_FIRST_ORDER = 150;
    const REDIRECT_ROUTE_PATH = 'checkout/cart/index';
    const REDIRECT_MESSAGE = 'You need to exceed $150 at your first order.';

    /** @var Session */
    private $checkoutSession;

    /** @var CollectionFactory */
    private $orderCollectionFactory;

    /** @var \Magento\Customer\Model\Session */
    private $customerSession;

    /** @var UrlInterface */
    private $url;

    /** @var ManagerInterface */
    private $messageManager;

    /**
     * Data constructor.
     * @param Session $session
     * @param Context $context
     */
    public function __construct(
        Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        CollectionFactory $orderCollectionFactory,
        UrlInterface $url,
        ManagerInterface $messageManager,
        Context $context
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->customerSession = $customerSession;
        $this->url = $url;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }

    /**
     * Returns order collection filtered by logged customers id
     * @return $this
     */
    public function getOrderCollection()
    {
        return $this->orderCollectionFactory->create()
            ->addAttributeToFilter('customer_id', $this->customerSession->getCustomerId())
            ->load()
            ->getItems();
    }

    /**
     * Check if customer/guest has reached minimum suborder value
     * @return bool
     */
    public function isMinimumSubtotalReached()
    {
        if (self::MINIMUM_SUBORDER_FOR_FIRST_ORDER <= $this->checkoutSession->getQuote()->getSubtotal()) {
            return true;
        }
        return false;
    }

    /**
     * Determine if customer is logged in
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }

    /**
     * Determine if quote has items
     * @return \Magento\Quote\Api\Data\CartItemInterface[]|mixed|null
     */
    public function hasItemsInQuote()
    {
        return $this->checkoutSession->getQuote()->getItems();
    }

    /**
     * Return url to cart
     * @return mixed
     */
    public function getCartUrl()
    {
       return $this->url->getUrl(self::REDIRECT_ROUTE_PATH);
    }

    /**
     * Add error message
     */
    public function addErrorMessage()
    {
        $this->messageManager->addError(self::REDIRECT_MESSAGE);
    }
}