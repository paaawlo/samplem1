<?php

namespace M2\FirstOrderMinimum\Observer;

use M2\FirstOrderMinimum\Helper\Data;
use Magento\Framework\App\ActionFlag;

/**
 * Class CanCheckout
 * @package M2\FirstOrderMinimum\Observer
 */
class CanExpressCheckout implements \Magento\Framework\Event\ObserverInterface
{
    /** @var Data */
    private $helper;

    /** @var ActionFlag */
    private $actionFlag;

    /**
     * CanExpressCheckout constructor.
     * @param Data $helper
     */
    public function __construct(
        ActionFlag $actionFlag,
        Data $helper
    )
    {
        $this->actionFlag = $actionFlag;
        $this->helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helper->hasItemsInQuote()) {
            return;
        }

        if (!$this->helper->isCustomerLoggedIn() && !$this->helper->isMinimumSubtotalReached()) {
            $this->helper->addErrorMessage();
            $this->stopExecutionAndRedirectToCart($observer);
            return;
        }

        if (!$this->helper->getOrderCollection() && !$this->helper->isMinimumSubtotalReached()) {
            $this->helper->addErrorMessage();
            $this->stopExecutionAndRedirectToCart($observer);
        }
    }

    /**
     * Stop executing controller after this dispatch event and redirect to cart
     * @param $observer
     */
    private function stopExecutionAndRedirectToCart(\Magento\Framework\Event\Observer $observer)
    {
        $this->actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);

        $observer->getControllerAction()
            ->getResponse()
            ->setRedirect($this->helper->getCartUrl());
    }
}