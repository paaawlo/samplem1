<?php

namespace M2\FirstOrderMinimum\Observer;

use M2\FirstOrderMinimum\Helper\Data;

/**
 * Class CanCheckout
 * @package M2\FirstOrderMinimum\Observer
 */
class CanCheckout implements \Magento\Framework\Event\ObserverInterface
{
    /** @var Data */
    private $helper;

    /**
     * CanCheckout constructor.
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    )
    {
        $this->helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->helper->hasItemsInQuote()) {
            return;
        }

        if (!$this->helper->isCustomerLoggedIn() && !$this->helper->isMinimumSubtotalReached()) {
            $this->helper->addErrorMessage();
            $this->redirectToCart($observer);
            return;
        }

        if (!$this->helper->getOrderCollection() && !$this->helper->isMinimumSubtotalReached()) {
            $this->helper->addErrorMessage();
            $this->redirectToCart($observer);
        }
    }

    /**
     * Perform redirect action to cart
     * @param $observer
     */
    private function redirectToCart(\Magento\Framework\Event\Observer $observer)
    {
        $observer->getControllerAction()
            ->getResponse()
            ->setRedirect($this->helper->getCartUrl());
    }
}