<?php

/**
 * Class Sample_FreeShipping_Model_Observer
 */
class Sample_FreeShipping_Model_Observer
{
    /**
     * @var Sample_FreeShipping_Helper_Data
     */
    private $helper;

    /**
     * Sample_FreeShipping_Model_Observer constructor.
     */
    public function __construct()
    {
        $this->helper = Mage::helper('freeshipping');
    }

    /**
     * Check if customer can checkout
     */
    public function canCheckout($observer)
    {
        $couponCode = $this->helper->getCouponCode();
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $checkoutRules = $this->helper->getCheckoutRules();

        if ($this->helper->isFreeShippingAvailable()) {
            return;
        }

        if ($quote->getCouponCode() == $couponCode['coupon_name'] && $quote->getSubtotal() >= $couponCode['checkout_required_amount']) {
            return;
        }

        $minimumOrder = $checkoutRules['checkout_from'];
        if ($quote->getSubtotal() < $minimumOrder) {
            $errorMessage = "Minimum order amount of $ $minimumOrder to order.";
            if ($quote->getCouponCode() == $couponCode['coupon_name']) {
                $errorMessage = "'" . $couponCode['coupon_name'] . "' coupon allows you to checkout for at least $" . $couponCode['checkout_required_amount'];
            }

            Mage::getSingleton('checkout/session')->addError($errorMessage);
            $observer->getControllerAction()->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));
            $observer->getControllerAction()->getResponse()->sendResponse();
            throw new Mage_Core_Controller_Varien_Exception($errorMessage);
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     */
    public function setFreeShipping(Varien_Event_Observer $observer)
    {
        if ($this->helper->isFreeShippingAvailable()) {
            $observer->getQuote()->getShippingAddress()->setFreeShipping(1);
        }
    }
}
