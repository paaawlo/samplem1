<?php

/**
 * Class Sample_FreeShipping_Block_Available
 */
class Sample_FreeShipping_Block_Shipping extends Amasty_Shiprules_Block_Cart_Shipping
{
    /**
     * Estimate shipping rates
     * @return array
     */
    public function getEstimateRates()
    {
        $helper = Mage::helper('freeshipping');

        if (!empty($this->_rates)) {
            return $this->_rates;
        }
        $groups = $this->getAddress()->getGroupedAllShippingRates();

        if (!$helper->isFreeShippingAvailable()) {
            foreach ($groups as $methodCode => $method) {
                if ($methodCode == $helper::FREE_SHIPPING_CODE) {
                    unset($groups[$methodCode]);
                }
            }
        }

        if ($helper->isFreeShippingAvailable()) {
            foreach ($groups as $methodCode => $method) {
                if ($methodCode != $helper::FREE_SHIPPING_CODE) {
                    unset($groups[$methodCode]);
                }
            }
        }

        // checking methods visibility for customer groups
        foreach ($groups as $methodCode => $method) {
            if (!Mage::helper('ammethods')->canUseMethod($methodCode, 'shipping')) {
                unset($groups[$methodCode]);
            }
        }

       return $this->_rates = $groups;
    }
}