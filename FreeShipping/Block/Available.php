<?php

/**
 * Class Sample_FreeShipping_Block_Available
 */
class Sample_FreeShipping_Block_Available extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{
   /**
     * @return array
     * @throws Exception
     */
    public function getShippingRates()
    {
        if (!empty($this->_rates)) {
            return $this->_rates;
        }

        $helper = Mage::helper('freeshipping');

        $this->getAddress()->collectShippingRates()->save();
        $groups = $this->getAddress()->getGroupedAllShippingRates();

        if (!$helper->isFreeShippingAvailable()) {
            foreach ($groups as $methodCode => $method) {
                if ($methodCode == $helper::FREE_SHIPPING_CODE) {
                    unset($groups[$methodCode]);
                }
            }
        }

        if ($helper->isFreeShippingAvailable()) {
            foreach ($groups as $methodCode => $method) {
                if ($methodCode != $helper::FREE_SHIPPING_CODE) {
                    unset($groups[$methodCode]);
                }
            }
        }

        // checking methods visibility for customer groups
        foreach ($groups as $methodCode => $method) {
            if (!Mage::helper('ammethods')->canUseMethod($methodCode, 'shipping')) {
                unset($groups[$methodCode]);
            }
        }

        return $this->_rates = $groups;
    }
}
