<?php

class Sample_FreeShipping_Helper_Data extends Mage_Core_Helper_Abstract
{
    /** code of free shipping method  */
    CONST FREE_SHIPPING_CODE = 'freeshipping';

    /**
     * @var array
     */
    private $orderRules;

    /**
     * @var array
     */
    private $checkoutRules;

    /**
     * @var array
     */
    private $couponCode;

    /**
     * @return array
     */
    public function getOrderRules()
    {
        return $this->orderRules;
    }

    /**
     * @return array
     */
    public function getCheckoutRules()
    {
        return $this->checkoutRules;
    }

    /**
     * @return array
     */
    public function getCouponCode(){
        return $this->couponCode;
    }

    /**
     * Sample_FreeShipping_Helper_Data constructor.
     */
    public function __construct()
    {
        $this->orderRules = Mage::getStoreConfig('rules/order_rules');
        $this->checkoutRules = Mage::getStoreConfig('rules/checkout_rules');
        $this->couponCode = Mage::getStoreConfig('coupon_code/checkout_coupon');
    }

    /**
     * Check if free shipping method is available for this quote
     * @return bool
     */
    public function isFreeShippingAvailable()
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        $startDate = date("Y-m-d H:i:s", strtotime('-' . $this->orderRules['hours_back'] . ' hours'));
        $endDate = date("Y-m-d H:i:s");

        $collection = Mage::getModel('sales/order')->getCollection();
        $collection->getSelect()->join(array('grid' => 'sales_flat_order_grid'), 'grid.entity_id = main_table.entity_id', array('grid.grand_total'));
        $collection->addFieldToSelect('shipping_amount')
            ->addFieldToFilter('customer_id', $quote->getCustomerId())
            ->addFieldToFilter('grid.grand_total', array('gteq' => $this->orderRules['order_amount']))
            ->addFieldToFilter('main_table.created_at', array('from' => $startDate, 'to' => $endDate))
            ->setOrder('main_table.created_at', 'desc');

        $orders = $collection->getData();

        foreach ($orders as $order) {
            $subTotal = $order['grand_total'] - $order['shipping_amount'];
            if ($subTotal >= $this->orderRules['order_amount']) {
                return true;
            }
        }

        return false;
    }
}